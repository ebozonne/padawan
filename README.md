# Admin

## before arrival for invited researcher or internship

- [ ] Convention chercheur invité (invited researcher agreement): document from Camille.Moinard@univ-lr.fr - signatures
- [ ] Safety listing (fiche sécurité): document from Camille - complete with Christelle.Rebere@univ-lr.fr
- [ ] Office space - Mathieu.Lemaire@univ-lr.fr (8624)
- [ ] Visa procedure - Isabelle Gaubert

**[Guide_anglais_Convention_accueil_prefecture_researcher.doc](:/755337e25d6c29d9cfb59ce3327994cb)**

## lab secretariat / arrival

- [ ] Installation (arrival/office room + key + access card pro + get office furnitures/leaving): Camille Moinard
- [ ] Security document: inform Camille (Xavier) + complete the document with Christelle Rebere
- [ ] Mandatory information lecture about lab security: half a day - periodic organization - do not miss the session given by Christelle Rebere
- [ ] computer purchase: ask informatique.lasie@univ-lr.fr (discuss the spec - available [on the intranet](https://doc.univ-lr.fr/nuxeo/nxdoc/default/17695b32-9181-4006-af87-ce68bd56d6b8/view_documents) ) > get a price & online demand by Karine.Planchet@univ-lr.fr (gestionnaire_lasie@univ-lr.fr in CC)

## Research account (Secretary: Camille Moinard)

Usually the following tasks are managed by Camille, just check after some weeks if anything is missing and if you receive emails addressed to the team and to the lab

- [ ] login/password for research intranet account
- [ ] in the lab mailing list + the team (E2) mailing list
- [ ] Lab member list on the website ( [http://lasie.univ-lr.fr](http://lasie.univ-lr.fr/) ) in the correct team with online CV + photo

## Travels (missions)

### Mission demand process

1.  check the budget line with supervisor,
    
2.  send a email request to gestionnaire_lasie@univ-lr.fr AND Karine Planchet with the mission details (time, location, etc) - never buy the tickets
    

## PhD school secretariat (for phd only)

- [ ] scholar fees and student intranet account
- [ ] redirect to human resource for salary (both require documents)
- [ ] look for PhD courses - most useful ones: Python (base + data analysis), bibliography tools (zotero, database)

## Intranet

https://intranet.univ-lr.fr/group/univ-larochelle/annuaire et https://intranet.univ-lr.fr/  
http://ent.univ-lr.fr/  
https://aide.wiki.univ-lr.fr/

# Computer setup & use

- Purchase computer (see admin - computer quote: DT and informatique.lasie@univ-lr.fr)  
- Setup printers and emails: see https://aide.wiki.univ-lr.fr/  
- Access online virtual PC for some software (TRNSYS) without necessary installation: https://view.univ-lr.fr/ (use `genie civil` vm for all software)

## software

### Writing and bibliography software

- Bibliography: Zotero standalone
- MS word: document writing rules to review/check (automatic legends/titles/numbering, illustration insertion, equation writting, etc…) > see `writing.pdf`

### Python software

1.  SETUP Python 3 (Anaconda - almost all included)
    - /!\\ JUST AFTER anaconda installation /!\\ : setup `conda-forge` as the defaut source for conda lib (IMPORTANT for future compatibility issues and more up to date usual lib than defaut anaconda sources)
        - `conda config --add channels conda-forge  
            conda config --set channel_priority strict`
            
        - check if it’s ok:  
            `conda config --get channels`
            
    - INSTALLATION of other libraries  
        NEVER use `pip install …` > ONLY `conda install …`
2.  IDE python
    - NOTEPAD++ simple code/text editor with automatic syntax color depending on the language + file comparison plugin
    - SPYDER (included) > prototyping (command lines with interactive documentation and visual variable content)
    - JUPYTERLAB (notebooks) > explain/keep documented scripts (Jupyterlab == userfriendly version of Python notebook)
        - install jupyterlab-desktop: https://github.com/jupyterlab/jupyterlab-desktop
    - PyCHARM > long codes (>debug mode is not option) | pro version is free for academics

### Learn Pandas
my own fast examples: https://colab.research.google.com/drive/1Sc3RKYoQAuATMaa8P9xqQqsxiZ49LLNY?usp=sharing

### Other software

- Save and update code versions (without multiple copies) >> GIT
    - login (univ account) on [https://gitlab.univ-lr.fr](https://gitlab.univ-lr.fr/)
    - install and configure the GIT for your project (existing or a new one) + GIT client (TortoiseGIT or GitKraken)
    - regular updates on the server with correct documentation
- Inkscape: vector graph + open PDF documents (eg. open vector graph included in pdf for clean edition before reuse)
- Sketchup Make (free version): for 3D modeling (simulation mockup)
- (N)ecessary/(U)seful free/opensource software:
    - (N) Windows pin https://github.com/navossoc/MenuTools/releases
    - (N) Paste without formating everywhere (CTRL+SHIFT+V): http://stevemiller.net/PureText/
    - (N) Translate on the fly in all apps (shortcut CTRL+CTRL)
        - QTranslate (best but devt discontinued): https://www.toutlibre.com/index.php?post/QTranslate
        - Crow (best alternative, missing double tanslation & Deepl): https://crow-translate.github.io/
    - (U) Write correct english (online): https://www.deepl.com/en/write
    - (U) launcher (shortcut ALT+SPC): http://keypirinha.com/
    - (U) Take notes (best opensource alternative to evernote): JOPLIN (https://joplinapp.org/)
        - Sync with nextcloud: create a folder joplinnote in nextcloud, and select webdav in joplin sync options (ao.univ-lr.fr)
        - Enable `Ignore TLS certificate errors` in advanced synchronization settings.
    - (U) SVG file preview in windows file explorer:  https://github.com/tibold/svg-explorer-extension/releases
    - (U) Boost windows startup time: https://www.chemtable.com/autorun-organizer.htm
    - (N) Acurate Printscreen far better than defaut windows PrtScr (useful for bibliography or article writing): Greenshot https://getgreenshot.org/downloads/

## cloud backup and document collaboration

- nextcloud login to create account (https://ao.univ-lr.fr)
- install nextcloud client / join shared folder for the project : automatic sync with the PC folders
- share big documents and reports via nextcloud links and not within the emails (save email size and energy)
- for document revisions rename versions (v1, v2, etc…) to avoid conflicts > see writing review process hereafter

## automatic file backup

- get a portable usb hard drive / encrypt
- setup automatic daily save (a fast and powerful open source program [https://freefilesync.org](https://freefilesync.org/) - setup folders’ mirror save + batch file + WINDOWS task planner to run batch at time)

# **Publications**

## **Writing process**

## **Writing review process (versions)**

Le numéro de révision que les journaux scientifiques utilisent par défaut, c’est

- *R*0 pour la version initiale
    
- *R*1, …, *R*n ensuite à chaque nouvelle ressoumission d’une version **R**évisée avec les réponses aux **R**eviewers
    

Le numéro de version c’est le numéro incrémenté par le premier auteur après chaque retour des corrections des collègues, ex. (doctorant \[LS\] auteur principal + encadrant \[YG\]) :

1.  LS (l’auteur principal) m’envoie sa 1ère version `article_LS_R0_v1.docx`
2.  Je lui renvoie ma version révisée de la *v1* en suivi de modif avec mes initiales Y*G* `article_LS_R0_v1_YG.docx`
3.  LS commence **d’abord par enregistrer** dans un nouveau doc  `article_LS_R0_v2.docx`
4.  LS valide chaque modification et fait ses propres modifs en mode révision + répond aux commentaires (sans les effacer)
5.  On peut alors itérer en (1) avec seulement les parties modifiées à relire/corriger et sans la surcharge des corrections précédentes (déjà validées)
6.  question subsidiaire qui sont LS et YG… 😉

## Bibliography

- Collect articles
    - … from ELSEVIER for free via sciencedirect.com accessible from Univ intranet (ent.univ-lr.fr) / direct link :  [https://www-sciencedirect-com.gutenberg.univ-lr.fr](https://www-sciencedirect-com.gutenberg.univ-lr.fr/)
    - … sci-hub is not l3g4l though there are almost all publications there (web address might change, https://sci-hub.se/, https://sci-hub.st/, https://sci-hub.ru/)
- Building Physics & Standards
    - … ISO, European and French standards + technical documentation:  http://flexlm.univ-lr.fr:8280/reef4/ (!!! only via INTRANET in University, no wifi or external access)
    - … “Techniques de l’ingénieur” (French engineer encyclopedia):  [http://www.techniques-ingenieur.fr.gutenberg.univ-lr.fr](http://www.techniques-ingenieur.fr.gutenberg.univ-lr.fr/)
- ZOTERO (cf. software install)
    - Collect online bibliography with firefox OR chrome extension for ZOTERO
    - join and maintain shared online ZOTERO folder (change/update if necessary collection names - check duplicates)
    - add in a separate collection la Rochelle univ references (self ref) + a sub-collection for self publications
    - useful ZOTERO plugins:
        - statistiques citations https://github.com/scitedotai/scite-zotero-plugin/releases
- *… TODO: add here rules to write articles*

## Articles and Conferences

- add all new publication in HAL (mandatory for the lab):
    
    - for journal publications DOI number auto-complete most of the fields (!! for conference publications with DOI the paper is modified to journal paper - correct this)
    - for conference publications complete manually all the fields
    - SHARE THE PROPERTY with all the coauthors (if further modifications are required)

## Publishers to avoid and possible journals

- Impact factors: https://www.scijournal.org/
- Predators (https://predatoryreports.org/), open access to avoid (Frontiersin - [fun in the news](https://venturebeat.com/ai/science-journal-retracts-peer-reviewed-article-containing-ai-generated-nonsensical-images/)), …
- Open access with correct impact factors at MDPI: Energies, Atmosphere
- Elsevier & al: energy&building, building&environment, urban climate, sustainable cities&society, building simulation, Journal of Building Performance Simulation

