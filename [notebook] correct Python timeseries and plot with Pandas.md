# DATA (pour l'exemple)

Test this notebook online https://colab.research.google.com/drive/1Sc3RKYoQAuATMaa8P9xqQqsxiZ49LLNY?usp=sharing

## online data source URL


```python
#2007-2021
laRochelle = "https://climate.onebuilding.org/WMO_Region_6_Europe/FRA_France/AC_Nouvelle-Aquitaine/FRA_AC_La.Rochelle.Intl.AP.073160_TMYx.2007-2021.zip"
Grenoble = "https://climate.onebuilding.org/WMO_Region_6_Europe/FRA_France/AR_Auvergne-Rhone-Alpes/FRA_AR_Grenoble.Alpes.Isere.AP.074860_TMYx.2007-2021.zip"
Carpentras = "https://climate.onebuilding.org/WMO_Region_6_Europe/FRA_France/PR_Provence-Alpes-Cote_d-Azur/FRA_PR_Carpentras.075860_TMYx.2007-2021.zip"
Nantes = "https://climate.onebuilding.org/WMO_Region_6_Europe/FRA_France/PL_Pays_de_la_Loire/FRA_PL_Nantes.Atlantique.AP.072220_TMYx.2007-2021.zip"
#2004-2018
Carpentras2004 = "https://climate.onebuilding.org/WMO_Region_6_Europe/FRA_France/PR_Provence-Alpes-Cote_d-Azur/FRA_PR_Carpentras.075860_TMYx.2004-2018.zip"
#TMY
CarpentrasTMY = "https://climate.onebuilding.org/WMO_Region_6_Europe/FRA_France/PR_Provence-Alpes-Cote_d-Azur/FRA_PR_Carpentras.075860_TMYx.zip"
```

## weather data select & donwload


```python
from zipfile import ZipFile
from io import BytesIO
from urllib.request import urlopen

r=urlopen(laRochelle)
zippedFiles = ZipFile(BytesIO(r.read()))
for name in zippedFiles.namelist():
    print(name)

rainFileName = [nomFichier for nomFichier in zippedFiles.namelist() if "rain" in nomFichier][0]
epwFileName =  [nomFichier for nomFichier in zippedFiles.namelist() if "epw" in nomFichier][0]
rainFileCSV = zippedFiles.open(rainFileName)
```

    FRA_AC_La.Rochelle.Intl.AP.073160_TMYx.2007-2021.clm
    FRA_AC_La.Rochelle.Intl.AP.073160_TMYx.2007-2021.ddy
    FRA_AC_La.Rochelle.Intl.AP.073160_TMYx.2007-2021.epw
    FRA_AC_La.Rochelle.Intl.AP.073160_TMYx.2007-2021.pvsyst
    FRA_AC_La.Rochelle.Intl.AP.073160_TMYx.2007-2021.rain
    FRA_AC_La.Rochelle.Intl.AP.073160_TMYx.2007-2021.stat
    FRA_AC_La.Rochelle.Intl.AP.073160_TMYx.2007-2021.wea
    



# Rain data

## load data
(read a CSV to make a proper Pandas timeserie)


```python
rainFileCSV = zippedFiles.open(rainFileName) # the CSV data file (only one column hourly data, without specified timestep)
print(rainFileName)
import pandas as pd
# for usual offline use replace 'rainFileCSV' by the filename string (zippedFiles.open function here because it is within an online zipped archive)
data=pd.read_csv(rainFileCSV, names=['rain'], skiprows=1) #skip 1st row as it only describes the data
data.index=pd.to_datetime(data.index,unit='h') # index to datetime easy as the first row (index= 1) is 1h00 on january 1st
data.rain=data.rain*1000 #convert in mm
data.head(4)
```

    FRA_AC_La.Rochelle.Intl.AP.073160_TMYx.2007-2021.rain
    




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>rain</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1970-01-01 00:00:00</th>
      <td>0.0</td>
    </tr>
    <tr>
      <th>1970-01-01 01:00:00</th>
      <td>0.0</td>
    </tr>
    <tr>
      <th>1970-01-01 02:00:00</th>
      <td>0.0</td>
    </tr>
    <tr>
      <th>1970-01-01 03:00:00</th>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
</div>



## Learn to plot with Pandas data
(without matplotlib, only high level "programming", no import, only one line)


```python
import seaborn as sns # easy management of plot style
sns.set_theme(context="notebook",style="whitegrid",palette="Blues") # for documents, articles, thesis, better use context = "poster"

data["1970-04-12":"1970-05-12"].rain.plot(figsize=[20,4],legend=False,ylabel='$R_\mathrm{rainfall}$ [mm]') # easy to select a specific period if necessary (or data.plot() for a fast plot of all columns)
```

    <>:4: SyntaxWarning: invalid escape sequence '\m'
    <>:4: SyntaxWarning: invalid escape sequence '\m'
    C:\Users\ebozonne\AppData\Local\Temp\ipykernel_15152\1203758860.py:4: SyntaxWarning: invalid escape sequence '\m'
      data["1970-04-12":"1970-05-12"].rain.plot(figsize=[20,4],legend=False,ylabel='$R_\mathrm{rainfall}$ [mm]') # easy to select a specific period if necessary (or data.plot() for a fast plot of all columns)
    




    <Axes: ylabel='$R_\\mathrm{rainfall}$ [mm]'>




    
![png](output_11_2.png)
    


## Heatmap and correct daily data plot for a full year


```python
data['hourOfTheDay'] = data.index.hour
data['days'] = pd.to_datetime(data.index.date)
rainPerDay = pd.pivot_table(data, values="rain", index=["hourOfTheDay"], columns=["days"])
rainMinMax = pd.pivot_table(data, values="rain", index=["days"], aggfunc= [sum, min, max])
data.head(4)
```

    C:\Users\ebozonne\AppData\Local\Temp\ipykernel_15152\2361283731.py:4: FutureWarning: The provided callable <built-in function sum> is currently using DataFrameGroupBy.sum. In a future version of pandas, the provided callable will be used directly. To keep current behavior pass the string "sum" instead.
      rainMinMax = pd.pivot_table(data, values="rain", index=["days"], aggfunc= [sum, min, max])
    C:\Users\ebozonne\AppData\Local\Temp\ipykernel_15152\2361283731.py:4: FutureWarning: The provided callable <built-in function min> is currently using DataFrameGroupBy.min. In a future version of pandas, the provided callable will be used directly. To keep current behavior pass the string "min" instead.
      rainMinMax = pd.pivot_table(data, values="rain", index=["days"], aggfunc= [sum, min, max])
    C:\Users\ebozonne\AppData\Local\Temp\ipykernel_15152\2361283731.py:4: FutureWarning: The provided callable <built-in function max> is currently using DataFrameGroupBy.max. In a future version of pandas, the provided callable will be used directly. To keep current behavior pass the string "max" instead.
      rainMinMax = pd.pivot_table(data, values="rain", index=["days"], aggfunc= [sum, min, max])
    




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>rain</th>
      <th>hourOfTheDay</th>
      <th>days</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1970-01-01 00:00:00</th>
      <td>0.0</td>
      <td>0</td>
      <td>1970-01-01</td>
    </tr>
    <tr>
      <th>1970-01-01 01:00:00</th>
      <td>0.0</td>
      <td>1</td>
      <td>1970-01-01</td>
    </tr>
    <tr>
      <th>1970-01-01 02:00:00</th>
      <td>0.0</td>
      <td>2</td>
      <td>1970-01-01</td>
    </tr>
    <tr>
      <th>1970-01-01 03:00:00</th>
      <td>0.0</td>
      <td>3</td>
      <td>1970-01-01</td>
    </tr>
  </tbody>
</table>
</div>




```python
import matplotlib.pyplot as plt

cmap = plt.cm.Blues
f, (ax0, ax1) = plt.subplots(2, 1, figsize=(20, 5), gridspec_kw = {'height_ratios':[0.5, 2], 'hspace':0})
rainMinMax['sum'].plot(ax=ax0,title="rain mm/day",kind='bar',sharex=True,legend=False,edgecolor=cmap(200),color=cmap(200),snap=False)
sns.heatmap(rainPerDay, xticklabels=30, yticklabels=6, ax=ax1, cmap=cmap,
            cbar_ax=f.add_axes([.92,.13,.01,.5]), cbar_kws={'use_gridspec':True,
                                                            'label': "rain [mm]"})
#mise en forme finale:
ax1.invert_yaxis()
ax1.xaxis.set_ticklabels([d.strftime('%d-%b') for d in rainPerDay.columns[ax1.get_xticks().astype(int)]])
```




    [Text(0.5, 0, '01-Jan'),
     Text(30.5, 0, '31-Jan'),
     Text(60.5, 0, '02-Mar'),
     Text(90.5, 0, '01-Apr'),
     Text(120.5, 0, '01-May'),
     Text(150.5, 0, '31-May'),
     Text(180.5, 0, '30-Jun'),
     Text(210.5, 0, '30-Jul'),
     Text(240.5, 0, '29-Aug'),
     Text(270.5, 0, '28-Sep'),
     Text(300.5, 0, '28-Oct'),
     Text(330.5, 0, '27-Nov'),
     Text(360.5, 0, '27-Dec')]




    
![png](output_14_1.png)
    


# Weather data EPW

## EPW file to dataframe
(lire un fichier .csv et le mettre proprement en timeserie)


```python
print(epwFileName)
epwFileCSV = zippedFiles.open(epwFileName) # the text data file in epw file format
import pandas as pd
# for usual offline use replace 'epwFileCSV' by the filename string (zippedFiles.open function here because it is within an online zipped archive)
dataepw=pd.read_csv(epwFileCSV,skiprows=8,index_col=False,
                    names=['year', 'month', 'day', 'hour', 'minute', 'datasource', 'dryBulb', 'dewPoint', 'RH', 'patm', 'Eo', 'Io', 'EGLOHoriz', 'Ehoriz', 'Ib', 'IdHoriz', 'LUXhoriz', 'LUXb', 'LUXd', 'LUXzenith', 'windDir', 'windSpeed', 'skyCover', 'vis', 'ceil', 'obs', 'code', 'precipWater', 'aerosol', 'snow', 'snowDays', 'albedo', 'rainDepht', 'rainQty'])
dataepw["hourOfTheYear"]=dataepw.index
dataepw.index = pd.to_datetime(dataepw.index,unit='h')
dataepw.head(3)
```

    FRA_AC_La.Rochelle.Intl.AP.073160_TMYx.2007-2021.epw
    

    C:\Users\ebozonne\AppData\Local\Temp\ipykernel_15152\1238386221.py:5: ParserWarning: Length of header or names does not match length of data. This leads to a loss of data with index_col=False.
      dataepw=pd.read_csv(epwFileCSV,skiprows=8,index_col=False,
    




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>year</th>
      <th>month</th>
      <th>day</th>
      <th>hour</th>
      <th>minute</th>
      <th>datasource</th>
      <th>dryBulb</th>
      <th>dewPoint</th>
      <th>RH</th>
      <th>patm</th>
      <th>...</th>
      <th>obs</th>
      <th>code</th>
      <th>precipWater</th>
      <th>aerosol</th>
      <th>snow</th>
      <th>snowDays</th>
      <th>albedo</th>
      <th>rainDepht</th>
      <th>rainQty</th>
      <th>hourOfTheYear</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1970-01-01 00:00:00</th>
      <td>2008</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>?9?9?9?9E0?9?9?9*9*9?9?9?9?9?9?9?9?9*9*9?9*9</td>
      <td>2.1</td>
      <td>2.0</td>
      <td>99</td>
      <td>102551</td>
      <td>...</td>
      <td>15</td>
      <td>9</td>
      <td>999999999</td>
      <td>12</td>
      <td>0.085</td>
      <td>0</td>
      <td>88</td>
      <td>0.09</td>
      <td>0.0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1970-01-01 01:00:00</th>
      <td>2008</td>
      <td>1</td>
      <td>1</td>
      <td>2</td>
      <td>0</td>
      <td>?9?9?9?9E0?9?9?9*9*9?9?9?9?9?9?9?9?9*9*9?9*9</td>
      <td>2.2</td>
      <td>2.1</td>
      <td>99</td>
      <td>102481</td>
      <td>...</td>
      <td>15</td>
      <td>9</td>
      <td>999999999</td>
      <td>12</td>
      <td>0.085</td>
      <td>0</td>
      <td>88</td>
      <td>0.09</td>
      <td>0.0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1970-01-01 02:00:00</th>
      <td>2008</td>
      <td>1</td>
      <td>1</td>
      <td>3</td>
      <td>0</td>
      <td>?9?9?9?9E0?9?9?9*9*9?9?9?9?9?9?9?9?9*9*9?9*9</td>
      <td>2.7</td>
      <td>1.3</td>
      <td>99</td>
      <td>102420</td>
      <td>...</td>
      <td>15</td>
      <td>9</td>
      <td>999999999</td>
      <td>11</td>
      <td>0.085</td>
      <td>0</td>
      <td>88</td>
      <td>0.09</td>
      <td>0.0</td>
      <td>2</td>
    </tr>
  </tbody>
</table>
<p>3 rows × 35 columns</p>
</div>




```python
dataepw["1970-04-12":"1970-05-12"].dryBulb.plot(figsize=[20,4],legend=False,ylabel='$T_\mathrm{ext}$ [°C]') # easy to select a specific period if necessary (or data.plot() for a fast plot of all columns)
```

    <>:1: SyntaxWarning: invalid escape sequence '\m'
    <>:1: SyntaxWarning: invalid escape sequence '\m'
    C:\Users\ebozonne\AppData\Local\Temp\ipykernel_15152\641175877.py:1: SyntaxWarning: invalid escape sequence '\m'
      dataepw["1970-04-12":"1970-05-12"].dryBulb.plot(figsize=[20,4],legend=False,ylabel='$T_\mathrm{ext}$ [°C]') # easy to select a specific period if necessary (or data.plot() for a fast plot of all columns)
    




    <Axes: ylabel='$T_\\mathrm{ext}$ [°C]'>




    
![png](output_18_2.png)
    


## Making a nice gradient of curves (z variation of underground temperatures)


```python
import numpy as np

def d(lbda=0.5, rhocp=1.5E6):
  return(np.sqrt(2*lbda/rhocp*366*24*3600/2/np.pi))

def Tground(t, z, Tmoy=13.3,Tmin=4.1,coldDay=30, lbda=0.5, rhocp=1.5E6):
  return(Tmoy-(Tmoy-Tmin)*np.exp(-z/d(lbda,rhocp))*np.cos((t-24*coldDay)*2*np.pi/366/24-z/d(lbda,rhocp)))

TgroundMoy = dataepw.dryBulb.mean()
Tdaily = dataepw.dryBulb.resample('d').mean()
TgroundMin = Tdaily.min()
coldestDay = list(Tdaily).index(TgroundMin)
```


```python
Ng = 50 #courbes
listGroundTemp = [dataepw.apply(lambda row: Tground(row['hourOfTheYear'],z,Tmoy=TgroundMoy,Tmin=TgroundMin, coldDay=coldestDay),axis=1) for z in [i/Ng*10. for i in range(Ng)]]
import matplotlib.pyplot as plt
listeCouleurs=[plt.cm.magma(i) for i in np.linspace(0,1,len(listGroundTemp))]
fig,ax = plt.subplots(figsize=(10,4))
for (dataground,couleur) in zip(listGroundTemp,listeCouleurs): dataground.plot(ax=ax,color=couleur)
```


    
![png](output_21_0.png)
    



```python

```
